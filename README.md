```postgresql
-- Есть 3 связанных таблицы:
-- Процедуры (id, наименование)
-- Лоты (Id, procedure_id [ссылка на таблицу процедуры], НДС)
-- Заказчики (Id, lot_id [ссылка на таблицу лоты], НДС заказчика)
-- Надо написать SQL-запрос, который бы выводил
-- наименование процедуры | суммарный НДС из лотов | суммарный НДС, уплаченный заказчиками.

tektorg=# SELECT p.name, l.procedure_id, sum(l.nds) as lot_nds_sum, sum(c.nds) as customer_nds_sum
tektorg-# FROM "procedures" p
tektorg-# INNER JOIN "lots" l ON p.id = l.procedure_id
tektorg-# INNER JOIN "customers" c ON c.lot_id = l.id
tektorg-# group by p.name, l.procedure_id
tektorg-# order by p.name;

    name     | procedure_id | lot_nds_sum | customer_nds_sum
-------------+--------------+-------------+------------------
 Процедура 1 |            1 |           1 |               10
 Процедура 2 |            2 |          22 |               80
 Процедура 3 |            3 |          43 |              120
(3 rows)
```