-- Adminer 4.6.2 PostgreSQL dump

\connect "tektorg";

INSERT INTO "customers" ("id", "lot_id", "nds") VALUES
(1,	1,	10),
(2,	2,	20),
(3,	3,	30),
(4,	4,	40),
(5,	5,	50),
(6,	6,	60);

INSERT INTO "lots" ("id", "procedure_id", "nds") VALUES
(1,	1,	1),
(2,	2,	2),
(3,	3,	3),
(4,	3,	30),
(5,	3,	10),
(6,	2,	20);

INSERT INTO "procedures" ("id", "name") VALUES
(1,	'Процедура 1'),
(2,	'Процедура 2'),
(3,	'Процедура 3'),
(4,	'Процедура 4'),
(5,	'Процедура 5');

-- 2018-11-16 14:27:59.690606+03
