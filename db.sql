-- Adminer 4.6.2 PostgreSQL dump

\connect "tektorg";

DROP TABLE IF EXISTS "customers";
DROP SEQUENCE IF EXISTS customers_id_seq;
CREATE SEQUENCE customers_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START  CACHE 1;

CREATE TABLE "public"."customers" (
    "id" integer DEFAULT nextval('customers_id_seq') NOT NULL,
    "lot_id" integer NOT NULL,
    "nds" integer NOT NULL,
    CONSTRAINT "customers_id" PRIMARY KEY ("id"),
    CONSTRAINT "customers_lot_id_fkey" FOREIGN KEY (lot_id) REFERENCES lots(id) NOT DEFERRABLE
) WITH (oids = false);

CREATE INDEX "customers_lot_id" ON "public"."customers" USING btree ("lot_id");


DROP TABLE IF EXISTS "lots";
DROP SEQUENCE IF EXISTS lots_id_seq;
CREATE SEQUENCE lots_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START  CACHE 1;

CREATE TABLE "public"."lots" (
    "id" integer DEFAULT nextval('lots_id_seq') NOT NULL,
    "procedure_id" integer NOT NULL,
    "nds" integer NOT NULL,
    CONSTRAINT "lots_id" PRIMARY KEY ("id"),
    CONSTRAINT "lots_procedure_id_fkey" FOREIGN KEY (procedure_id) REFERENCES procedures(id) NOT DEFERRABLE
) WITH (oids = false);

CREATE INDEX "lots_procedure_id" ON "public"."lots" USING btree ("procedure_id");


DROP TABLE IF EXISTS "procedures";
DROP SEQUENCE IF EXISTS procedures_id_seq;
CREATE SEQUENCE procedures_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START  CACHE 1;

CREATE TABLE "public"."procedures" (
    "id" integer DEFAULT nextval('procedures_id_seq') NOT NULL,
    "name" character varying(255) NOT NULL,
    CONSTRAINT "procedures_id" PRIMARY KEY ("id")
) WITH (oids = false);


-- 2018-11-16 14:15:27.884866+03
