
SELECT p.name, l.procedure_id, sum(l.nds) as lot_nds_sum, sum(c.nds) as customer_nds_sum
FROM "procedures" p
INNER JOIN "lots" l ON p.id = l.procedure_id
INNER JOIN "customers" c ON c.lot_id = l.id
group by p.name, l.procedure_id
order by p.name;